import os

instances = 4
src_path = "/home/andrei/twitch/RichardLewisReports"
# MKV must be at least half as big as the MP4
size_tol = 0.5

hevc = {}
h264 = {}
for file in os.listdir(src_path):
    # Skip btn
    if 'return of' in file.lower():
        print(f'SKIPPING BTN {file}')
        continue
    file_size = os.path.getsize(f'{src_path}/{file}')
    if file.endswith('.mp4'):
        h264[file[:-4]] = file_size
    elif file.endswith('.mkv'):
        hevc[file[:-4]] = file_size

done_lst = []
failed_lst = []
if os.path.exists('status'):
    with open(f'status', 'r', encoding='utf-8') as fr:
        for line in fr:
            if not os.path.exists(f'{line.replace("DONE: ", "")}.mp4'):
                continue
            if line.startswith('DONE'):
                done_lst.append(line.split('/')[-1].replace('\n', ''))
            elif line.startswith('FAIL'):
                failed_lst.append(line.split('/')[-1].replace('\n', ''))

conv = {}
for k, v in h264.items():
    if k in hevc:
        if hevc[k] / v < size_tol:
            print(f'SIZE MISMATCH: MKV/MP4 {hevc[k]/1e9:,.1f}GB/{v/1e9:,.1f}GB: {k}')

for k, v in h264.items():
    to_add = True
    # Check if it has been converted
    if k in done_lst:
        print(f'ALREADY DONE: {k}')
        continue
    elif k in failed_lst:
        print(f'PREVIOUSLY FAILED: {k}')
    conv[k] = v

per_instance_size = sum(conv.values()) / instances
print(f'{instances} instances, each converting {per_instance_size/1e9:,.1f}GB')
per_instance_files = [[] for i in range(instances)]
i = 0
curr_size = 0
for k, v in conv.items():
    per_instance_files[i].append(os.path.join(src_path, k))
    curr_size += v
    if curr_size >= per_instance_size and i < instances:
        print(f'instance {i} will convert {curr_size/1e9:,.1f}GB')
        i += 1
        curr_size = 0

print(f'instance {i} will convert {curr_size/1e9:,.1f}GB')

for i in range(instances):
    with open(f'instance_{i}.in', 'w', encoding='utf-8') as fw:
        for file in per_instance_files[i]:
            # fw.write(f'ffmpeg -y -i "{file}.mp4" -c:v libx265 -crf 23 -preset:v medium -c:a copy "{file}.mkv"\n')
            fw.write(f'{file}\n')

if len(done_lst) > 0 and input('Delete completed files? ').lower() == 'y':
    for d in done_lst:
        full_path = os.path.join(src_path, f'{d}.mp4')
        try:
            os.unlink(full_path)
            print(f'Deleted {full_path}:')
        except Exception as e:
            print(f'Cannot delete {full_path}: {str(e)}')
# print(f'### ALREADY COMPLETED ###\n{hevc_list}')
# print(f'### TO CONVERT ###\n{conv_list}')
