#!/bin/bash

while IFS="" read -r p || [ -n "$p" ]
do
    ffmpeg -y -threads 12 -i "$p.mp4" -c:v libx265 -crf 23 -preset:v medium -c:a copy "$p.mkv" </dev/null
    if [[ $? -ne 0 ]]; then
        echo "FAIL: $p" >> status
    else
	echo "DONE: $p" >> status
    fi
done < "$1"
