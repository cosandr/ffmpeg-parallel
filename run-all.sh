#!/bin/bash

python3 ./dist-files.py
read -p "Press enter to continue"
touch status
tmux \
    new-session  "./run-instance.sh instance_0.in ; read" \; \
    split-window "./run-instance.sh instance_1.in ; read" \; \
    split-window "./run-instance.sh instance_2.in ; read" \; \
    split-window "./run-instance.sh instance_3.in ; read" \; \
    select-layout even-vertical

